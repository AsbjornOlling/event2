import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";
import pluginRewriteAll from "vite-plugin-rewrite-all";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [svelte(), pluginRewriteAll()],
});
