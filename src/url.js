// @ts-nocheck
/* Reactive URLs in Svelte
 * Kinda bloaty but eh, this seemed like the simplest way to get reactive urls.
 * I'm not really ready to take on sveltekit yet. * ¯\_(ツ)_/¯
 * Downloaded from: https://github.com/bluwy/svelte-url/blob/dbde32b0ff842603a269b9506127fc2be63054fb/src/url.js
 * There's a blogpost to go with it here: https://bjornlu.com/blog/simple-svelte-routing-with-reactive-urls
 */
import { derived, writable } from "svelte/store";

export function createUrlStore(ssrUrl) {
  // Ideally a bundler constant so that it's tree-shakable
  if (typeof window === "undefined") {
    const { subscribe } = writable(ssrUrl);
    return { subscribe };
  }

  const href = writable(window.location.href);

  const originalPushState = history.pushState;
  const originalReplaceState = history.replaceState;

  const updateHref = () => href.set(window.location.href);

  history.pushState = function () {
    originalPushState.apply(this, arguments);
    updateHref();
  };

  history.replaceState = function () {
    originalReplaceState.apply(this, arguments);
    updateHref();
  };

  window.addEventListener("popstate", updateHref);
  window.addEventListener("hashchange", updateHref);

  return {
    subscribe: derived(href, ($href) => new URL($href)).subscribe,
  };
}

// If you're using in a pure SPA, you can return a store directly and share it everywhere
export default createUrlStore();
