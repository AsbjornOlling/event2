import * as sdk from "matrix-js-sdk";

export interface CalendarEvent {
  type: "chat.cactus.calendar.event";
  content: CalendarEventContent;
}

export interface CalendarEventContent {
  "chat.cactus.calendar.event": {
    title: string;
    description: string;
    begin: number;
    end: number;
  };
  "m.message"?: Array<{
    mimetype: "text/markdown" | "text/html" | "text/plain";
    body: string;
  }>;
  "m.location"?: {
    uri: string;
    description: string;
  };
  "m.image"?: {
    uri: string;
  };
}

export const isCalendarEvent = (x: any): x is CalendarEvent => {
  // TODO make this check more thorough
  return (
    x &&
    x["type"] == "chat.cactus.calendar.event" &&
    isCalendarEventContent(x["content"])
  );
};

export const isCalendarEventContent = (x: any): x is CalendarEventContent => {
  return (
    x &&
    typeof x["chat.cactus.calendar.event"]["title"] == "string" &&
    typeof x["chat.cactus.calendar.event"]["description"] == "string" &&
    typeof x["chat.cactus.calendar.event"]["begin"] == "number" &&
    typeof x["chat.cactus.calendar.event"]["end"] == "number"
  );
};

export const createEventRoom = async (
  client: sdk.MatrixClient,
  data: { title: string; description: string; begin: Date; end: Date }
): Promise<{ roomId: string; eventId: string }> => {
  let { title, description, begin, end } = data;

  // form the content of the matrix event
  const eventContent: CalendarEventContent = {
    "chat.cactus.calendar.event": {
      title: title,
      description: description,
      begin: begin.getTime(), // UTC time
      end: end.getTime(),
    },
  };

  // make the room
  const room = await client.createRoom({
    visibility: sdk.Visibility.Public,
    invite: [],
    name: title,
    topic: description, // TODO: truncate to fit max state event length?
  });

  // put one event in it
  const event = await client.sendEvent(
    room.room_id,
    "chat.cactus.calendar.event",
    eventContent
  );

  return { roomId: room.room_id, eventId: event.event_id };
};

export const getCalendarEvent = async (
  client: sdk.MatrixClient,
  roomId: string,
  eventId: string
): Promise<CalendarEvent> => {
  const event = await client.fetchRoomEvent(roomId, eventId);
  if (!isCalendarEvent(event))
    throw new Error("Could not parse calendar event.");
  return event;
};
