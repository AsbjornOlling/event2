import * as sdk from "matrix-js-sdk";

const GUEST_HOMESERVER = import.meta.env.VITE_GUEST_HOMESERVER || "cactus.chat";

type UserId = { localpart: string; servername: string };

export function parseUserId(userIdInput: string): UserId | undefined {
  /**
   * Parses a userid strings into localpart and servername.
   * Returns `undefined` if parsing fails.
   * @param {string} userIdInput - User input, an un-parsed userid string.
   * @returns {UserId | undefined} - Parsed result, `undefined` if parse fails.
   */
  if (!userIdInput.startsWith("@")) return undefined;
  if (!userIdInput.includes(":")) return undefined;
  const localpart: string = userIdInput.split(":")[0].slice(1);
  const servername: string = userIdInput.split(":").slice(1).join(":");
  if (!localpart || !servername) return undefined;

  // parse success!
  return { localpart: localpart, servername: servername };
}

async function lookupHomeserver(servername: string): Promise<string> {
  // look up homeserver base url from userid servername
  const clientConfig: sdk.IClientWellKnown =
    await sdk.AutoDiscovery.findClientConfig(servername);
  const hsUrl: string | null | undefined =
    clientConfig?.["m.homeserver"]?.base_url;
  if (!hsUrl)
    throw new Error("No homeserver url found in .well-known response");
  return hsUrl;
}

export async function login(
  userIdInput: string,
  password: string
): Promise<sdk.MatrixClient> {
  /**
   * Parses login form data and tries to log in to a Matrix server.
   */
  // parse user id
  const userId: UserId | undefined = parseUserId(userIdInput);
  if (!userId) throw Error("UserId did not parse.");

  // find homeserver url based on userid servername
  const homeserverUrl: string = await lookupHomeserver(userId.servername);

  // create a client and log in
  const client: sdk.MatrixClient = sdk.createClient(homeserverUrl);
  await client.loginWithPassword(userId.localpart, password);

  // start the client
  await client.startClient({ disablePresence: true });

  return client;
}

interface PersistedLoginData {
  baseUrl: string;
  accessToken: string;
  userId: string;
  guest: boolean;
}

function isPersistedLoginData(obj: any): obj is PersistedLoginData {
  return (
    obj.baseUrl &&
    obj.accessToken &&
    obj.userId &&
    typeof obj.guest === "boolean"
  );
}

export async function loadClient(): Promise<sdk.MatrixClient | undefined> {
  // initialize `MatrixClient` from localstorage data if available
  const unparsed: string | null = localStorage.getItem("loginData");
  if (!unparsed) return;
  const parsed: any = JSON.parse(unparsed);
  if (!isPersistedLoginData(parsed)) return;
  const client = sdk.createClient(parsed);
  client.setGuest(parsed.guest);
  await client.startClient({ disablePresence: true });
  return client;
}

export function saveClient(c: sdk.MatrixClient): void {
  // save login data to localstorage
  let data: object = {
    baseUrl: c.getHomeserverUrl(),
    accessToken: c.getAccessToken(),
    userId: c.getUserId(),
    guest: c.isGuest(),
  };
  if (!isPersistedLoginData(data))
    throw new Error("Failed creating PersistedLoginData from client object.");
  localStorage.setItem("loginData", JSON.stringify(data));
}

export async function registerGuest(): Promise<sdk.MatrixClient> {
  // register a guest user on `GUEST_HOMESERVER`
  const homeserverUrl: string = await lookupHomeserver(GUEST_HOMESERVER);
  const tmpClient = sdk.createClient(homeserverUrl);
  const guestCreds = await tmpClient.registerGuest({});
  const client = sdk.createClient({
    baseUrl: homeserverUrl,
    accessToken: guestCreds.access_token,
    userId: guestCreds.user_id,
    deviceId: guestCreds.device_id,
  });
  client.setGuest(true);
  await client.startClient({ disablePresence: true });
  return client;
}
