import "./index.css";
import App from "./App.svelte";

const appElem = document.getElementById("app");
if (!appElem) throw new Error("Could not find element with id `app`.");
const app = new App({
  target: appElem,
});

export default app;
