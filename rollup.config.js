import sveltePreprocess from "svelte-preprocess";

module.exports = {
  preprocess: sveltePreprocess({
    sourceMap: production,
    postcss: {
      plugins: [require("tailwindcss"), require("autoprefixer")],
    },
  }),
};
